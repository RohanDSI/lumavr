import { LumaSplatsSemantics,LumaSplatsThree } from "@lumaai/luma-web";
import { VRButton } from "three/examples/jsm/webxr/VRButton.js";
import { DemoProps } from ".";
// import { Box3, Camera, CubeTexture, InstancedBufferGeometry, Mesh, ShaderMaterial, Sphere, Vector3, WebGLRenderer } from "three";
import * as THREE from "three";

export function DemoVR(props: DemoProps) {
	let { renderer, camera, scene, controls, gui } = props;
 
	renderer.xr.enabled = true;

	let vrButton = VRButton.createButton(renderer);
	let canvas = renderer.getContext().canvas as HTMLCanvasElement;
	canvas.parentElement!.append(vrButton);

	// Extract URL parameter
	const urlParams = new URLSearchParams(window.location.hash.split('?')[1]);
	var url = urlParams.get('sceneid');
	// console.log("url**")
	// console.log(url)
	// console.log(urlParams)
	if (url==null){
		url = "c4247bc1-b313-4694-8efc-147f57cd323f"
	}
	let splats = new LumaSplatsThree({
		// Kind Humanoid @RyanHickman
		// source: 'https://lumalabs.ai/capture/83e9aae8-7023-448e-83a6-53ccb377ec86',
		// source: 'C:\\Users\\rohan\\Downloads\\murder_scene_point_cloud.ply',
		source: "https://lumalabs.ai/capture/"+url,
		// disable three.js shader integration for performance
		enableThreeShaderIntegration: false,
	});

	scene.add(splats);
	  
	
	let layersEnabled = {
		Background: false,
		Foreground: true,
	}
	function updateSemanticMask() {
		splats.semanticsMask =
			(layersEnabled.Background ? LumaSplatsSemantics.BACKGROUND : 0) |
			(layersEnabled.Foreground ? LumaSplatsSemantics.FOREGROUND : 0);
	}
	updateSemanticMask();
	gui.add(layersEnabled, 'Background').onChange(updateSemanticMask);

	
	
	// //add annotation markers
	// var markerA = new THREE.Mesh(
	// 	new THREE.SphereGeometry(0.1, 10, 20),
	// 	new THREE.MeshBasicMaterial({
	// 	color: 0xff5555
	// 	})
	// );
	// var markerB = markerA.clone();
	// var markers = [
	// 	markerA, markerB
	// ];
	// scene.add(markerA);
	// scene.add(markerB);
	
	// var lineGeometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(), new THREE.Vector3()]);
	// var lineMaterial = new THREE.LineBasicMaterial({
	// color: 0xff5555
	// });
	// var line = new THREE.Line(lineGeometry, lineMaterial);
	// scene.add(line);


	 // Function to add a ball to the scene
	//  function addBall(event) {
    //     const ballGeometry = new THREE.SphereGeometry(0.5, 32, 32);
    //     const ballMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 });
    //     const ball = new THREE.Mesh(ballGeometry, ballMaterial);

    //     // Calculate click position in 3D space
    //     const mouse = new THREE.Vector2();
    //     mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    //     mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    //     const raycaster = new THREE.Raycaster();
    //     raycaster.setFromCamera(mouse, camera);
    //     const intersects = raycaster.intersectObjects(scene.children);
    //     if (intersects.length > 0) {
    //         const point = intersects[0].point;
    //         ball.position.copy(point);
    //     } else {
    //         // If no intersection found, place the ball at a default position
	// 		console.log("If no intersection found");
    //         ball.position.set(0, 0, -10);
    //     }

    //     scene.add(ball);
    // }
    // // Event listener for adding a ball on click
    // renderer.domElement.addEventListener('click', addBall);


	return {
		dispose: () => {
			splats.dispose();
			vrButton.remove();
		}
	}
}